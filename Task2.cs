using System;

namespace Task2
{
	class Rectangle
	{
		private double _xLeft;
		private double _yTop;
		private double _xRight;
		private double _yBottom;

		public Rectangle(double xLeft = new double(), double yTop = new double(), double xRight = new double(), double yBottom = new double())
		{
			_xLeft = xLeft;
			_yTop = yTop;
			_xRight = xRight;
			_yBottom = yBottom;
		}

		public Rectangle(Rectangle r)
		{
			_xLeft = r._xLeft;
			_yTop = r._yTop;
			_xRight = r._xRight;
			_yBottom = r._yBottom;	
		}

		public double XLeft
		{
			get {return _xLeft; }
			set 
			{									
				if(value > _xRight)
				{
					_xLeft = _xRight;
					_xRight = value;
				}
				else
				{
					_xLeft = value;
				}
			}
		}

		public double XRight
		{
			get {return _xRight; }
			set 
			{
				if(value < _xLeft)
				{
					_xRight = _xLeft;
					_xLeft = value;
				}
				else
				{
					_xRight = value;
				}
			}
		}

		public double YBottom
		{
			get {return _yBottom; }
			set 
			{				
				if(value > _yTop)
				{
					_yBottom = _yTop;
					_yTop = value;
				}
				else
				{
					_yBottom = value;
				}
			}
		}

		public double YTop
		{
			get {return _yTop; }
			set 
			{
				if(value < _yBottom)
				{
					_yTop = _yBottom;
					_yBottom = value;
				}
				else
				{
					_yTop = value;
				}
			}
		}

		public void MoveLeft(double step)
		{
			_xLeft -= step;
			_xRight -= step;
		}

		public void MoveRight(double step)
		{
			_xLeft += step;
			_xRight += step;
		}

		public void MoveUp(double step)
		{
			_yTop += step;
			_yBottom += step;
		}

		public void MoveDown(double step)
		{
			_yTop -= step;
			_yBottom -= step;
		}

		public Rectangle UnionRectangles(Rectangle r)
		{
			return new Rectangle(Math.Min(_xLeft, r._xLeft), Math.Max(_yTop, r._yTop), Math.Max(_xRight, r._xRight), Math.Min(_yBottom, r._yBottom));
		}

		public Rectangle IntersectionRectangles(Rectangle r)
		{
			if((_xRight < r._xLeft) || (_xLeft > r._xRight) || (_yTop < r._yBottom) || (_yBottom > r._yTop))
			{
				throw new Exception("Rectangles haven't joint area.");
			}
			return new Rectangle(Math.Max(_xLeft, r._xLeft), Math.Min(_yTop, r._yTop), Math.Min(_xRight, r._xRight), Math.Max(_yBottom, r._yBottom));
		}

		public override string ToString()
		{
			return String.Format("({0} {1} {2} {3})", _xLeft, _yTop, _xRight, _yBottom);
		}
	}

	class Pogram
	{
		static void Main()
		{
			try
			{
				Console.WriteLine("Rectangles will be show like (XLeft, YTop, XRight, YBottom)");

				Rectangle r1 = new Rectangle(3, 3, 3, 3);
				Rectangle r2 = new Rectangle(2, 4, 5, 1);
				
				Console.WriteLine("r1 = {0}", r1);

				r1.XRight = 5;
				Console.WriteLine("r1 after change XRight = {0}", r1);

				r1.YBottom = 1;
				Console.WriteLine("r1 after change YBottom = {0}", r1);

				r1.XLeft = 2;
				Console.WriteLine("r1 after change xLeft = {0}", r1);

				r1.YTop = -2;
				Console.WriteLine("r1 after change yTop = {0}", r1);
				Console.WriteLine();

				r1.MoveUp(4);
				Console.WriteLine("r1 after move up = {0}", r1);
				r1.MoveLeft(1);
				Console.WriteLine("r1 after move left = {0}", r1);
				Console.WriteLine();
				
				Console.WriteLine("r1 = {0}", r1);
				Console.WriteLine("r2 = {0}", r2);
				Console.WriteLine();

				Console.WriteLine("Rectangle that contain r1 and r2 = {0}", r1.UnionRectangles(r2));
				Console.WriteLine("Joint area of r1 and r2 = {0}", r1.IntersectionRectangles(r2));
				Console.WriteLine();

				r1.MoveRight(4);
				Console.WriteLine("r1 after move right = {0}", r1);
				r1.MoveDown(5);
				Console.WriteLine("r1 after move down = {0}", r1);
				Console.WriteLine();
				
				Console.WriteLine("r1 = {0}", r1);
				Console.WriteLine("r2 = {0}", r2);
				Console.WriteLine();

				Console.WriteLine("Joint area of r1 and r2 = {0}", r1.IntersectionRectangles(r2));
				Console.WriteLine();
			}
			catch(Exception e)
			{
				Console.WriteLine(e);
			}			
		}
	}
}