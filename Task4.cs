using System;

namespace Task4
{

	class Matrix
	{
		private double[,] _matrix;

		public Matrix(double[,] matrix = null)
		{
			_matrix = new double[matrix.GetLength(0), matrix.GetLength(1)];

			for(int i = 0; i < matrix.GetLength(0); i++)
			{
				for(int j = 0; j < matrix.GetLength(1); j++)
				{
					_matrix[i, j] = matrix[i, j];
				}
			}
		}

		public Matrix(int firstDimension, int secondDimension, double initValue = new double())
		{
			_matrix = new double[firstDimension, secondDimension];

			for(int i = 0; i < _matrix.GetLength(0); i++)
			{
				for(int j = 0; j < _matrix.GetLength(1); j++)
				{
					_matrix[i, j] = initValue;
				}
			}
		}		

		public Matrix(Matrix matrix)
		{
			_matrix = new double[matrix._matrix.GetLength(0), matrix._matrix.GetLength(1)];

			for(int i = 0; i < matrix._matrix.GetLength(0); i++)
			{
				for(int j = 0; j < matrix._matrix.GetLength(1); j++)
				{
					_matrix[i, j] = matrix._matrix[i, j];
				}
			}
		}

		public int FirstDimension()
		{
			return _matrix.GetLength(0);
		}

		public int SecondDimension()
		{
			return _matrix.GetLength(1);
		}

		public double this[int i, int j]
		{
			get
			{
				return _matrix[i, j];
			}

			set
			{
				_matrix[i, j] = value;
			}
		}

		private bool IsEqualSize(Matrix matrix)
		{
			return ((this.FirstDimension() == matrix.FirstDimension()) && (this.SecondDimension() == matrix.SecondDimension()));
		}

		public Matrix Add(Matrix matrix)
		{
			if(!this.IsEqualSize(matrix))
			{
				throw new Exception("Matrices haven't equal size.");
			}

			for(int i = 0; i < this.FirstDimension(); i++)
			{
				for(int j = 0; j < this.SecondDimension(); j++)
				{
					this[i, j] += matrix[i, j];
				}
			}

			return this;
		}

		public Matrix Subtract(Matrix matrix)
		{
			if(!this.IsEqualSize(matrix))
			{
				throw new Exception("Matrices haven't equal size.");
			}

			for(int i = 0; i < this.FirstDimension(); i++)
			{
				for(int j = 0; j < this.SecondDimension(); j++)
				{
					this[i, j] -= matrix[i, j];
				}
			}

			return this;
		}

		public Matrix Multiply(Matrix matrix)
		{
			if(this.SecondDimension() != matrix.FirstDimension())
			{
				throw new Exception("Matrices haven't required size.");
			}

			Matrix tempFirstMatrix = new Matrix(this);

			_matrix = new double[this.FirstDimension(), matrix.SecondDimension()];

			for(int i = 0; i < this.FirstDimension(); i++)
			{
				for(int j = 0; j < this.SecondDimension(); j++)
				{
					double sum = 0.0;
					for(int k = 0; k < tempFirstMatrix.SecondDimension(); k++)
					{
						sum += tempFirstMatrix[i, k] * matrix[k, j];
					}
					
					this[i, j] = sum;
				}
			}			

			return this;
		}

		public Matrix MultiplyByScalar(double scalar)
		{
			for(int i = 0; i < this.FirstDimension(); i++)
			{
				for(int j = 0; j < this.SecondDimension(); j++)
				{
					this[i, j] *= scalar;
				}
			}

			return this;
		}

		public static Matrix MultiplyByScalar(Matrix matrix, double scalar)
		{
			Matrix result = new Matrix(matrix);
			for(int i = 0; i < result.FirstDimension(); i++)
			{
				for(int j = 0; j < result.SecondDimension(); j++)
				{
					result[i, j] *= scalar;
				}
			}

			return result;
		}

		public Matrix Transpose()
		{
			Matrix tempMatrix = new Matrix(this);
			
			_matrix = new double[tempMatrix.SecondDimension(), tempMatrix.FirstDimension()];

			for(int i = 0; i < tempMatrix.FirstDimension(); i++)
			{
				for(int j = 0; j < tempMatrix.SecondDimension(); j++)
				{
					this[j, i] = tempMatrix[i, j];
				}
			}			

			return this;
		}

		public static Matrix Transpose(Matrix matrix)
		{
			Matrix result = new Matrix(matrix.SecondDimension(), matrix.FirstDimension());

			for(int i = 0; i < result.FirstDimension(); i++)
			{
				for(int j = 0; j < result.SecondDimension(); j++)
				{
					result[i, j] = matrix[j, i];
				}
			}

			return result;
		}

		public static Matrix GetSubMatrix(Matrix matrix, int firstDimension, int secondDimension)
		{
			if((firstDimension > matrix.FirstDimension()) || (firstDimension < 0) || (secondDimension > matrix.SecondDimension()) || (secondDimension < 0))
			{
				throw new Exception("Can't obtain submatrix. Bad arguments.");
			}

			Matrix result = new Matrix(firstDimension, secondDimension);

			for(int i = 0; i < result.FirstDimension(); i++)
			{
				for(int j = 0; j < result.SecondDimension(); j++)
				{
					result[i, j] = matrix[i, j];
				}
			}

			return result;
		}

		public Matrix GetSubMatrix(int firstDimension, int secondDimension)
		{
			if((firstDimension > this.FirstDimension()) || (firstDimension < 0) || (secondDimension > this.SecondDimension()) || (secondDimension < 0))
			{
				throw new Exception("Can't obtain submatrix. Bad arguments.");
			}

			Matrix tempMatrix = new Matrix(this);

			_matrix = new double[firstDimension, secondDimension];

			for(int i = 0; i < firstDimension; i++)
			{
				for(int j = 0; j < secondDimension; j++)
				{
					_matrix[i, j] = tempMatrix[i, j];
				}
			}

			return this;
		}

		public override bool Equals(Object obj)
		{
			if(obj == null)
			{
				return false;
			}

			Matrix matrix = obj as Matrix;

			if((Object)matrix == null)
			{
				return false;
			}

			return (this == matrix);
		}

		public override int GetHashCode()
		{
			int result = 0;

			for(int i = 0; i < this.FirstDimension(); i++)
			{
				for(int j = 0; j < this.SecondDimension(); j++)
				{
					result ^= Convert.ToInt32(this[i , j]);
				}
			}

			return result;
		}


		public static Matrix operator +(Matrix matrix1, Matrix matrix2)
		{
			if(!matrix1.IsEqualSize(matrix2))
			{
				throw new Exception("Matrices haven't equal size.");
			}

			Matrix result = new Matrix(matrix1.FirstDimension(), matrix1.SecondDimension());

			for(int i = 0; i < result.FirstDimension(); i++)
			{
				for(int j = 0; j < result.SecondDimension(); j++)
				{
					result[i, j] = matrix1[i, j] + matrix2[i, j];
				}
			}

			return result;
		}

		public static Matrix operator -(Matrix matrix1, Matrix matrix2)
		{
			if(!matrix1.IsEqualSize(matrix2))
			{
				throw new Exception("Matrices haven't equal size.");
			}

			Matrix result = new Matrix(matrix1.FirstDimension(), matrix1.SecondDimension());

			for(int i = 0; i < result.FirstDimension(); i++)
			{
				for(int j = 0; j < result.SecondDimension(); j++)
				{
					result[i, j] = matrix1[i, j] - matrix2[i, j];
				}
			}

			return result;	
		}

		public static Matrix operator *(Matrix matrix1, Matrix matrix2)
		{
			if(matrix1.SecondDimension() != matrix2.FirstDimension())
			{
				throw new Exception("Matrices haven't required size.");
			}

			Matrix result = new Matrix(matrix1.FirstDimension(), matrix2.SecondDimension());

			for(int i = 0; i < result.FirstDimension(); i++)
			{
				for(int j = 0; j < result.SecondDimension(); j++)
				{
					double sum = 0.0;
					for(int k = 0; k < matrix1.SecondDimension(); k++)
					{
						sum += matrix1[i, k] * matrix2[k, j];
					}
					
					result[i, j] = sum;
				}
			}

			return result;
		}

		public static bool operator ==(Matrix matrix1, Matrix matrix2)
		{
			if(!matrix1.IsEqualSize(matrix2))
			{
				return false;
			}

			for(int i = 0; i < matrix1.FirstDimension(); i++)
			{
				for(int j = 0; j < matrix1.SecondDimension(); j++)
				{
					if(matrix1[i, j] != matrix2[i, j])
					{
						return false;
					}
				}
			}

			return true;
		}

		public static bool operator!=(Matrix matrix1, Matrix matrix2)
		{
			return !(matrix1 == matrix2);
		}
	}
	
	class Program
	{

		static void PrintMatrix(Matrix matrix)
		{
			for(int i = 0; i < matrix.FirstDimension(); i++)
			{
				for(int j = 0; j < matrix.SecondDimension(); j++)
				{
					Console.Write("{0} ", matrix[i, j]);
				}
				Console.WriteLine();
			}
		}

		static void Main()
		{
			try
			{
				Matrix m1 = new Matrix(new double[,]{{1, 2}, {2, 3}, {3, 4}});
				Matrix m2 = new Matrix(new double[,]{{-1, -2}, {2, 3}, {-3, -4}});
				Matrix m3 = new Matrix(new double[,]{{1, 2, 3, 4}, {3, 4, 5, 6}});
				Matrix m4 = new Matrix(m1);

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("m3");
				PrintMatrix(m3);
				Console.WriteLine();
				Console.WriteLine("m4");
				PrintMatrix(m4);
				Console.WriteLine();

				Console.WriteLine("Is m1 == m2? {0}", m1 == m2);
				Console.WriteLine("Is m1 == m4? {0}", m1 == m4);
				Console.WriteLine();
			
				Console.WriteLine("Is m1 equel m2? {0}", m1.Equals(m2));
				Console.WriteLine("Is m1 equel m4? {0}", m1.Equals(m4));
				Console.WriteLine();
				
				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("(static)m2 after multiply by 2");
				PrintMatrix(Matrix.MultiplyByScalar(m2, 2));
				Console.WriteLine();
				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("m2 after multiply by 2");
				PrintMatrix(m2.MultiplyByScalar(2));
				Console.WriteLine();

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("m1 + m2 = ");
				PrintMatrix(m1 + m2);

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("m1 - m2 = ");
				PrintMatrix(m1 - m2);
				Console.WriteLine();

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("m3");
				PrintMatrix(m3);
				Console.WriteLine();
				Console.WriteLine("m1 * m3 = ");
				PrintMatrix(m1 * m3);
				Console.WriteLine();
			
				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("m1.Add(m2) = ");
				PrintMatrix(m1.Add(m2));

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("m1.Subtract(m2) = ");
				PrintMatrix(m1.Subtract(m2));
				Console.WriteLine();

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("m3");
				PrintMatrix(m3);
				Console.WriteLine();
				Console.WriteLine("m1.Multiply(m3) = ");
				PrintMatrix(m1.Multiply(m3));
				Console.WriteLine();

				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("(static)m2 after transpose = ");
				PrintMatrix(Matrix.Transpose(m2));
				Console.WriteLine();

				Console.WriteLine("m2");
				PrintMatrix(m2);
				Console.WriteLine();
				Console.WriteLine("m2 after transpose = ");
				PrintMatrix(m2.Transpose());
				Console.WriteLine();

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("(static)submatrix m1 2x2 = ");
				PrintMatrix(Matrix.GetSubMatrix(m1, 2, 2));
				Console.WriteLine();

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("submatrix m1 3x3 = ");
				PrintMatrix(m1.GetSubMatrix(3, 3));
				Console.WriteLine();

				Console.WriteLine("m1");
				PrintMatrix(m1);
				Console.WriteLine();
				Console.WriteLine("submatrix m1 4x4 = ");
				PrintMatrix(m1.GetSubMatrix(4, 4));
				Console.WriteLine();
			}
			catch(Exception e)
			{
				Console.WriteLine(e);
			}
		}
	}
}